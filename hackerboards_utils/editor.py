import os
import pathlib
import re
import sys
import unicodedata

import yaml
from rich.text import Text
from textual import on
from textual.app import App, ComposeResult
from textual.containers import Vertical, VerticalScroll, Grid, Horizontal
from textual.coordinate import Coordinate
from textual.screen import Screen, ModalScreen
from textual.suggester import SuggestFromList
from textual.widgets import Header, Footer, Static, Label, DataTable, Button, Input, Switch, Select, SelectionList
from textual.validation import Integer, Length, Number

from hackerboards_utils.schema import fields as schema


class CancelResult:
    pass


def slugify(raw):
    raw = raw.replace('+', ' plus')
    slug = unicodedata.normalize('NFKD', raw)
    slug = slug.encode('ascii', 'ignore').lower().decode()
    slug = re.sub(r'[^a-z0-9]+', '-', slug).strip('-')
    slug = re.sub(r'[-]+', '-', slug)
    return slug


def field_to_cell(field, value):
    if value is None:
        return ''

    if field['type'] == 'boolean':
        if value is None:
            value = ''
        elif value:
            value = 'yes'
        else:
            value = 'no'
    elif field['type'] == 'reference':
        value = f'[{value}]' if value is not None and value != '' else ''
    elif field['type'] == 'array':
        value = ', '.join(value)
    if 'unit' in field:
        if field['unit'] == 'USD':
            value = '$' + str(value / 100)
        elif field['unit'] == 'MHz':
            if value is None or value == 0:
                value = ""
            elif value < 1:
                value = f'{value * 1000} Khz'
            else:
                value = f'{value} Mhz'
        elif field['unit'] == 'B':
            if value is None or value == 0:
                value = ""
            elif value < 1024:
                value = f"{value} B"
            elif value < 1024 * 1024:
                value = f"{value / 1024} KiB"
            elif value < 1024 * 1024 * 1024:
                value = f"{value / 1024 / 1024} MiB"
            else:
                value = f"{value / 1024 / 1024 / 1024} GiB"
        elif field['unit'] == 'count':
            if value is None or value == '':
                value = ''
            elif value == 0:
                value = '-'
            else:
                value = f'{value}x'

    return str(value)


class MessageDialog(Screen):
    def __init__(self, message):
        super().__init__()
        self.message = message

    def compose(self) -> ComposeResult:
        yield Grid(
            Label(self.message, id="question", classes="wide"),
            Button("Close", variant="primary"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        self.pop_screen()


class ConfirmDialog(Screen):
    def __init__(self, message):
        super().__init__()
        self.message = message

    def compose(self) -> ComposeResult:
        yield Grid(
            Label(self.message, id="question", classes="wide"),
            Button("Yes", variant="error", id="yes"),
            Button("No", variant="primary", id="no"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        self.dismiss(event.button.id == "yes")


class ValueEdit(ModalScreen):
    def __init__(self, field, value):
        self.field = field
        self.value = value
        super().__init__()

    def on_input_changed(self, event: Input.Changed):
        if not event.validation_result.is_valid:
            return
        if self.field['type'] == 'integer':
            if 'unit' in self.field and self.field['unit'] == 'B':
                raw = event.value.upper()
                exponent = 'M'
                if 'T' in raw:
                    exponent = 'T'
                elif 'G' in raw:
                    exponent = 'G'
                elif 'K' in raw:
                    exponent = 'K'
                bits = 'bit' in raw.lower()
                num = re.sub(r'[^0-9.]', "", raw)
                base = float(num)
                if exponent == 'K':
                    base = base * 1024
                elif exponent == 'M':
                    base = base * 1024 * 1024
                elif exponent == 'G':
                    base = base * 1024 * 1024 * 1024
                elif exponent == 'T':
                    base = base * 1024 * 1024 * 1024 * 1024

                result = int(base)
                if bits:
                    result = result / 8
                self.value = result
            elif 'unit' in self.field and self.field['unit'] == 'USD':
                self.value = int(float(event.value) * 100)
            else:
                self.value = int(float(event.value))
        elif self.field['type'] == 'float':
            self.value = float(event.value)
        else:
            self.value = event.value

    def on_selection_list_selection_toggled(self, event: SelectionList.SelectionToggled):
        slist = event.selection_list
        self.value = slist.selected

    def on_input_submitted(self, event: Input.Submitted):
        self.dismiss(self.value)

    def on_switch_changed(self, event: Switch.Changed):
        self.value = event.value

    def on_select_changed(self, event: Select.Changed):
        self.value = event.value

    def compose(self) -> ComposeResult:
        tall = False
        if self.field['type'] in ['string', 'date']:
            widget = Input(str(self.value), classes="wide", validators=[Length(minimum=1)])
        elif self.field['type'] == 'boolean':
            widget = Switch(value=self.value, classes="wide")
        elif self.field['type'] == 'integer':
            if 'unit' in self.field and self.field['unit'] == 'B':
                value = field_to_cell(self.field, self.value)
                widget = Input(value, classes="wide", validators=[Length(minimum=1)])
            elif 'unit' in self.field and self.field['unit'] == 'USD':
                widget = Input(str(self.value / 100), validators=[Number(minimum=0)], classes='wide')
            else:
                widget = Input(str(self.value) if self.value is not None else "", classes="wide",
                               validators=[Integer(minimum=0)])

            if 'unit' in self.field:
                widget = Horizontal(widget, Vertical(Label(self.field['unit']), classes="unit"), classes="wide")
        elif self.field['type'] == 'float':
            widget = Input(str(self.value) if self.value is not None else "", classes="wide",
                           validators=[Number(minimum=0)])
            if 'unit' in self.field:
                widget = Horizontal(widget, Vertical(Label(self.field['unit']), classes="unit"), classes="wide")
        elif self.field['type'] == 'reference':
            datasource = self.field['relation']
            with open(datasource, 'r') as handle:
                lut = yaml.load(handle, yaml.CLoader)
            options = []
            for item in lut:
                options.append(item['name'])
            widget = Input(self.value, suggester=SuggestFromList(options, case_sensitive=True), classes="wide",
                           validators=[Length(minimum=1)])
        elif self.field['type'] == 'array':
            datasource = self.field['relation']
            with open(datasource, 'r') as handle:
                lut = yaml.load(handle, yaml.CLoader)
            options = []
            for item in lut:
                options.append((item['name'], item['name']))
            widget = SelectionList(*options, classes="list")
            for item in self.value:
                widget.select(item)
            tall = True
        else:
            raise RuntimeError(f"Unknown type {self.field['type']}")
        yield Grid(
            Label(f"Field {self.field['name']}", classes="wide"),
            widget,
            Label(self.field['label'] if 'label' in self.field else '', classes="wide"),
            Button("Cancel", variant="error", id="cancel"),
            Button("Save", variant="primary", id="save"),
            id="dialog-tall" if tall else "dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "cancel":
            self.dismiss(CancelResult)
        else:
            self.dismiss(self.value)


class EditorApp(App):
    TITLE = "Hackerboards Editor"
    CSS_PATH = "style.tcss"
    BINDINGS = [
        ("escape", "exit", "Exit"),
        ("s", "save", "Save"),
    ]

    def __init__(self, files):
        self.files = files
        self.data = {}
        self.dirty = False
        super().__init__()

    def compose(self) -> ComposeResult:
        yield Header()
        yield DataTable()
        yield Footer()

    def action_exit(self):
        if not self.dirty:
            exit(0)

        def on_result(result):
            if result:
                exit(0)

        self.push_screen(ConfirmDialog("Are you sure you want to quit without saving?"), on_result)

    def action_save(self):
        error = False
        for file in self.data:
            if self.data[file]['name'] is None or len(self.data[file]['name']) < 3:
                error = True
                break
            if self.data[file]['manufacturer'] is None or len(self.data[file]['manufacturer']) < 1:
                error = True

        if error:
            self.push_screen(MessageDialog("The board needs at least the name and manufacturer to be valid."))
            return

        refcache = {}

        for file in self.data:
            key = file
            if file == 'new':
                dirname = slugify(self.data[key]['manufacturer'])
                file = dirname + "/" + slugify(self.data[key]['name']) + ".yaml"
                if not os.path.isdir(dirname):
                    os.mkdir(dirname)

            for field in schema:
                if field['type'] == 'reference' and \
                        field['name'] in self.data[key] and \
                        self.data[key][field['name']] is not None:
                    if field['relation'] not in refcache:
                        with open(field['relation'], 'r') as handle:
                            loaded = yaml.load(handle, Loader=yaml.CLoader)
                            refcache[field['relation']] = loaded
                    for row in refcache[field['relation']]:
                        if row['name'] == self.data[key][field['name']]:
                            break
                    else:
                        refcache[field['relation']].append({
                            'name': self.data[key][field['name']],
                            'todo': 'FIX BEFORE COMMIT',
                        })

            with open(file, 'w') as handle:
                yaml.dump(self.data[key], handle, Dumper=yaml.CDumper)

        for file in refcache:
            data = sorted(refcache[file], key=lambda d: d['name'])
            with open(file, 'w') as handle:
                yaml.dump(data, handle, Dumper=yaml.CDumper)

        self.dirty = False

        table = self.query_one(DataTable)
        for column in table.columns:
            col = table.columns[column]
            col.label.style = ''

        for column in table.columns:
            for cell in table.get_column(column):
                if isinstance(cell, Text) and cell.style == '#FFFF66':
                    cell.style = ''

        # Redraw
        dummy = table.get_cell_at(Coordinate(0, 0))
        table.update_cell_at(Coordinate(0, 0), dummy)

    @on(DataTable.CellSelected)
    def on_cell_activate(self, event: DataTable.CellSelected):
        board = event.cell_key.column_key.value
        field_name = event.cell_key.row_key.value
        if field_name is None:
            return
        value = self.data[board][field_name]
        for field in schema:
            if field['name'] == field_name:
                break
        else:
            raise RuntimeError("Undefined field")

        def on_dialog_close(result):
            if result is CancelResult:
                return
            table = self.query_one(DataTable)

            # Save the value in a yellow cell
            dirty_cell = Text(field_to_cell(field, result), style='#FFFF66')
            table.update_cell(field_name, board, dirty_cell, update_width=True)

            # Set the column header yellow to signify the changed boards
            col = table.columns[board]
            col.label.style = '#FFFF66'

            self.dirty = True
            self.data[board][field_name] = result

        self.push_screen(ValueEdit(field, value), on_dialog_close)

    def on_mount(self):
        self.sub_title = "Edit board"

        file = None
        for file in self.files:
            if file is not None:
                break
        if file is not None:
            path = pathlib.Path(file)
            data_root = str(path.parent.parent.absolute())
            os.chdir(data_root)
        else:
            if not os.path.isfile('core.yaml'):
                raise RuntimeError("Not running from the dataset directory")

        table = self.query_one(DataTable)
        table.zebra_stripes = True

        for file in self.files:
            if file is None:
                table.add_column("New board", key='new')
            else:
                basename = os.path.basename(file)
                table.add_column(basename.replace(".yaml", ""), key=file)

        groups = {}
        for field in schema:
            if 'group' in field:
                if field['group'] not in groups:
                    groups[field['group']] = []
                groups[field['group']].append(field)

        self.data = {}
        for file in self.files:
            if file is not None:
                with open(file, 'r') as handle:
                    data = yaml.load(handle, yaml.CLoader)
                self.data[file] = data
            else:
                template = {}
                for field in schema:
                    template[field['name']] = field['default']
                self.data['new'] = template

        for group in groups:
            table.add_row(label=Text(group))
            for field in groups[group]:
                label = Text(str(field['name']), justify="right", style="italic #999999")
                row = []
                for file in self.files:
                    if file is None:
                        row.append(".")
                    else:
                        board = self.data[file]
                        value = None
                        if field['name'] in board:
                            value = board[field['name']]
                        value = field_to_cell(field, value)
                        row.append(value[:30] + "..." if len(value) > 30 else value)
                table.add_row(*row, label=label, key=field['name'])


def main():
    import argparse

    parser = argparse.ArgumentParser(description="Hackerboards Editor")
    parser.add_argument("filename", type=str, nargs='+')
    args = parser.parse_args()

    files = []
    for name in args.filename:
        if name == 'new':
            files.append(None)
        else:
            if not os.path.isfile(name):
                sys.stderr.write(f"File not found: {name}\n")
                exit(1)
            files.append(name)
    app = EditorApp(files=files)
    app.run()


if __name__ == '__main__':
    main()
